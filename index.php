<?php
include_once __DIR__ . '/Model/Produk.php';
$listProduk = Produk::getAll();
?>
<html lang="en">

<head>
    <title>Data Produk</title>
</head>

<body>
    <h1>Data Produk</h1>
    <table border="1" width='100%'>
        
            <tr>
                <th>ID</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Harga</th>
                <th>Stok</th>
                <th>Action</th>
            </tr>
            <?php foreach ($listProduk as $produk) { ?>
                <tr>
                    <td><?= $produk->id ?></td>
                    <td><?= $produk->kode ?></td>
                    <td><?= $produk->nama ?></td>
                    <td><?= $produk->harga ?></td>
                    <td><?= $produk->stok ?></td>
                    <td>
                        <a href="/view/formUbah.php?id=<?=$produk->id?>">Edit</a>
                    </td>
                </tr>
            <?php } ?>
    </table>
</body>

</html>