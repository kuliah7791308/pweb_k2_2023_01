create database pweb_k2_2023_01;
use pweb_k2_2023_01;

create table produk(
  id int not null primary key auto_increment,
  kode varchar(10) not null unique,
  nama varchar(150) not null,
  harga decimal(10,2) default 0,
  stok int default 0
);

/*insert data ke tabel produk*/
INSERT INTO `produk` VALUES (NULL,'P001','Spidol Snowman',1000,5);
INSERT INTO `produk` VALUES (NULL,'P002','Kertas A4',8.97,6);
