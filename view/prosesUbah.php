<?php
include_once __DIR__ . '/../Model/Produk.php';
if (isset($_REQUEST['id']) === false) {
    echo "Data tidak ditemukan.<br/> <a href='/index.php'>Kembali</a>";
    exit;
} else {
    $id = $_REQUEST['id'];
    $produk = Produk::getByPrimaryKey($id);
    if ($produk == null) {
        echo "Data tidak ditemukan.<br/> <a href='/index.php'>Kembali</a>";
        exit;
    }
}

#ambil semua parameter non-id
$kode = $_REQUEST['kode'];
$nama = $_REQUEST['nama'];
$harga = $_REQUEST['harga'];
$stok = $_REQUEST['stok'];

#set ke objek
$produk->kode = $kode;
$produk->nama = $nama;
$produk->harga = $harga;
$produk->stok = $stok;
#update data di database
$produk->update();

header('Location: /index.php');
