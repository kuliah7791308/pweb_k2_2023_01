<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FOrm Tambah</title>
</head>

<body>
    <h1>Tambah Produk</h1>
    <form action="prosesTambah.php" method="get">
        <p>
            Kode : <br>
            <input type="text" name="kode" required />
        </p>
        <p>
            Nama : <br>
            <input type="text" name="nama" required />
        </p>
        <p>
            Harga : <br>
            <input type="number" min='1' name="harga" required />
        </p>
        <p>
            Stok : <br>
            <input type="number" min="0" name="stok" required />
        </p>
        
        <button type="reset">Reset</button>
        <button type="submit">Simpan Data</button>
    </form>
</body>

</html>