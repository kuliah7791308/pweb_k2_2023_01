<?php
include_once __DIR__ . '/../Model/Produk.php';
#ambil semua parameter yang dikirim via form dan simpan ke variabel
$kode = $_REQUEST['kode'];
$nama = $_REQUEST['nama'];
$harga = $_REQUEST['harga'];
$stok = $_REQUEST['stok'];

#buat objek baru dari produk dan set semua fields nya
$produk = new Produk();
$produk->kode = $kode;
$produk->nama = $nama;
$produk->harga = $harga;
$produk->stok = $stok;
$produk->insert();

header('Location: /index.php');
