<?php
include_once __DIR__ . '/../Model/Produk.php';
if (isset($_REQUEST['id']) === false) {
    echo "Data tidak ditemukan.<br/> <a href='/index.php'>Kembali</a>";
    exit;
} else {
    $id = $_REQUEST['id'];
    $produk = Produk::getByPrimaryKey($id);
    if ($produk == null) {
        echo "Data tidak ditemukan.<br/> <a href='/index.php'>Kembali</a>";
        exit;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Ubah Produk</h1>
    <form action="prosesUbah.php" method="post">
        <p>
            Kode : <br>
            <input type="text" name="kode" value="<?= $produk->kode ?>" required />
        </p>
        <p>
            Nama : <br>
            <input type="text" name="nama" value="<?= $produk->nama ?>" required />
        </p>
        <p>
            Harga : <br>
            <input type="number" min='1' value="<?= $produk->harga ?>" name="harga" required />
        </p>
        <p>
            Stok : <br>
            <input type="number" min="0" value="<?= $produk->stok ?>" name="stok" required />
        </p>
        <input type="hidden" name="id" value="<?=$produk->id ?>">
        <a href="/index.php">Kembali</a>
        <button type="reset">Reset</button>
        <button type="submit">Simpan Data</button>
    </form>
</body>

</html>