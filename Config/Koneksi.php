<?php
class Koneksi
{
    private $host = "localhost"; // localhost atau alamat server
    private $user = "root";  // username database
    private $pass = "";   // password <PASSWORD>
    private $db_name = 'pweb_k2_2023_01';
    private $port = '3306';
    public $koneksi;

    public function __construct()
    {
        $this->koneksi = mysqli_connect(
            $this->host,
            $this->user,
            $this->pass,
            $this->db_name,
            $this->port
        );
    }
}
