<?php
include_once __DIR__ . '/../Config/Koneksi.php';
class Produk
{
    public $id;
    public $kode;
    public $nama;
    public $harga;
    public $stok;

    /**
     * Fungsi yang digunakan untuk mengambil semua data produk
     * @return Array
     */
    public static function getAll(): array
    {
        $query = 'select * from produk';
        $connection = new Koneksi();
        $mq = mysqli_query($connection->koneksi, $query);
        $result = [];
        while ($prDB = mysqli_fetch_object($mq)) {
            $pr = new Produk();
            $pr->id = $prDB->id;
            $pr->kode = $prDB->kode;
            $pr->nama = $prDB->nama;
            $pr->harga = $prDB->harga;
            $pr->stok = $prDB->stok;
            $result[] = $pr;
        }
        return $result;
    }

    public function insert()
    {
        $query = "insert into produk(kode,nama,harga,stok) values "
            . "('$this->kode','$this->nama','$this->harga','$this->stok')";
        $connection =  new Koneksi();
        mysqli_query($connection->koneksi, $query);
    }

    public static function getByPrimaryKey($id)
    {
        $query = "select * from produk where id='$id'";
        $connection =  new Koneksi();
        $mq = mysqli_query($connection->koneksi, $query);
        $result = null;
        while ($prDB = mysqli_fetch_object($mq)) {
            $produk = new Produk();
            $produk->id = $prDB->id;
            $produk->kode = $prDB->kode;
            $produk->nama = $prDB->nama;
            $produk->harga = $prDB->harga;
            $produk->stok = $prDB->stok;
            $result = $produk;
        }
        return $result;
    }

    public function update()
    {
        $query = "update produk set "
            . "kode = '$this->kode', "
            . "nama = '$this->nama', "
            . "harga = '$this->harga', "
            . "stok = '$this->stok' "
            . "where id='$this->id'";
            var_dump($query);
        $connection =  new Koneksi();
        mysqli_query($connection->koneksi, $query);
    }
}
